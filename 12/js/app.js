import Vue from 'vue';
import Card from "./Components/Card";

Vue.component('card', Card);

new Vue({
    el: '#app',
});