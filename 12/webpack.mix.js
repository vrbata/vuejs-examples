let mix = require('laravel-mix');

mix.js(['js/app.js'], 'app.js')
    .setPublicPath('public')
    .version();

mix.webpackConfig({
    module: {
        rules: [{
            test: /\.js?$/,
            exclude: /(bower_components)/,
            use: [{
                loader: 'babel-loader',
                options: mix.config.babel()
            }]
        }]
    }
});
